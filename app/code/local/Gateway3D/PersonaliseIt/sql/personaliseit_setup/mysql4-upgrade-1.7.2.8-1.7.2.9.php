<?php


$installer = $this;

$installer->addAttribute('catalog_product', 'personaliseit_is_ext_artwork', array(
	'section'			=> 'general',
	'group'             => 'Gateway3D Personalise-iT',
	'label'             => 'Is External Artwork',
	'type'              => 'int',
	'input'             => 'boolean',
	'default'           => '0',
	'class'             => '',
	'backend'           => '',
	'frontend'          => '',
	'source'            => 'eav/entity_attribute_source_table',
	'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
	'visible'           => true,
	'required'          => false,
	'user_defined'      => false,
	'searchable'        => false,
	'filterable'        => false,
	'comparable'        => false,
	'visible_on_front'  => false,
	'visible_in_advanced_search' => false,
	'unique'            => false
));


$installer->addAttribute('catalog_product', 'personaliseit_ext_artwork_url', array(
	'section'			=>'general',
	'group'             => 'Gateway3D Personalise-iT',
	'label'             => 'External Artwork URL',
	'type'              => 'varchar',
	'input'             => 'text',
	'default'           => '',
	'class'             => '',
	'backend'           => '',
	'frontend'          => '',
	'source'            => '',
	'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
	'visible'           => true,
	'required'          => false,
	'user_defined'      => false,
	'searchable'        => false,
	'filterable'        => false,
	'comparable'        => false,
	'visible_on_front'  => false,
	'visible_in_advanced_search' => false,
	'unique'            => false
));
