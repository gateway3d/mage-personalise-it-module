<?php
class Gateway3D_PersonaliseIt_Model_Observer {

	public function resendToOms() {
		$block = Mage::app()->getLayout()->getBlock('sales_order_edit');
	    if (!$block) {
	        return $this;
	    }
	    
	    $order = Mage::registry('current_order');
	    $url   = Mage::helper("adminhtml")->getUrl(
	        "adminhtml/resend",
	        array('order_id' => $order->getId())
	    );

	    $block->addButton(
	        'button_id',
	        array(
	            'label'   => Mage::helper('personaliseit')->__('Resend to OMS'),
	            'onclick' => 'setLocation(\'' . $url . '\')',
	            'class'   => 'go'
	        )
	    );
	    return $this;
	} 
}