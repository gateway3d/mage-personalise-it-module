  <?php

class Gateway3D_PersonaliseIt_ResendController extends Mage_Adminhtml_Controller_Action 
{
	public function indexAction()
	{		
		$orderId = $this->getRequest()->getParam('order_id');
		if ($orderId!='') {
			$currentOrder = Mage::getModel('sales/order')->load($orderId);
			$currentOrder->sent = 0;
			$currentOrder->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
			$currentOrder->save();
			
			Mage::getSingleton('adminhtml/session')->addSuccess('Sent status of this order was reset. Order will be pushed again to OMS in next cycle.');
			
			$this->_returnUrl = 'adminhtml/sales_order/view';
			$this->_redirect( $this->_returnUrl, array( 'order_id' => $orderId) );
		}
	}
 }